# 5.5 - Take Home Excersise
---

### 1 Additional notes

* The example request bodies (swagger API example) included an `id` which I assumed was the primary key for 
records in the DB. I assumed the bodies were only a boilerplate of the calls and due to this fact, I prototyped my API.
* Because it is just a prototype, I did use sample exceptions
* The ApiRequest model described in the example swagger API was handled as a build-in request macro
* I left the build-in exception messages in validation requests
* Authorization was handled through the official package Passport (Grant keys)

### 2 How was it set-up
#### 2.1 Database
* Based on postgresql
* Migrations and seeds included

#### 2.2 Deployment
* Based on docker container - did not bother to make a custom dockerfile, did base it on 
the ubuntu:latest image (prototype)
* Container architecture is based on Nginx reverse proxy by jwilder
* Inside the container, apache2 was used with the app

### 3 Setting up locally
* An sample .env file is included: `.env.sample.pedryc`
* `php artisan key:generate` to create a new session key on new .env file
* `sudo php artisan serve --host=127.0.0.1 --port=80`

### Contact
**For more information:**  
Email: jan.pedryc@gmail.com  
Phone: +48 727 698 376