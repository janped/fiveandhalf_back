<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhotoUrlsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add examples cat photos
        DB::table('photo_urls')->insert([
            'pet_id'    => 4,
            'url'       => asset('storage/4/first_pet.jpeg'),
        ]);
    }
}
