<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
           'pet_id'     => 2,
           'quantity'   => 2,
           'shipDate'   => Carbon::now()->addMonth(),
           'status'     => 'placed',
           'complete'   => false,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('orders')->insert([
            'pet_id'     => 3,
            'quantity'   => 1,
            'shipDate'   => Carbon::now()->subMonth(),
            'status'     => 'delivered',
            'complete'   => true,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
    }
}
