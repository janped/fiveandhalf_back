<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add sample pet - a cat
        DB::table('pets')->insert([
            'category_id'      => 2,
            'name'          => 'Miouwy',
            'status'        => 'available',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        // Add sample pet - a bird
        DB::table('pets')->insert([
            'category_id'      => 4,
            'name'          => 'Chipper',
            'status'        => 'pending',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        // Add sample pet - a horse
        DB::table('pets')->insert([
            'category_id'      => 3,
            'name'          => 'Robert',
            'status'        => 'sold',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        // Add sample pet - a dog
        DB::table('pets')->insert([
            'category_id'      => 1,
            'name'          => 'Barky',
            'status'        => 'available',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);
    }
}
