<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PetsTableSeeder::class);
        $this->call(PetTagsTableSeeder::class);
        $this->call(PhotoUrlsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
    }
}
