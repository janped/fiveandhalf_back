<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PetTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add the examples cat tags
        DB::table('pet_tags')->insert([
          'pet_id'  => 1,
          'tag_id'  => 2,
        ]);

        // Add the examples bird tags
        DB::table('pet_tags')->insert([
            'pet_id'  => 2,
            'tag_id'  => 2,
        ]);

        DB::table('pet_tags')->insert([
            'pet_id'  => 2,
            'tag_id'  => 4,
        ]);

        DB::table('pet_tags')->insert([
            'pet_id'  => 2,
            'tag_id'  => 5,
        ]);

        // Add the examples horse tags
        DB::table('pet_tags')->insert([
            'pet_id'  => 3,
            'tag_id'  => 1,
        ]);

        // Add the examlpes dog tags
        DB::table('pet_tags')->insert([
            'pet_id'  => 4,
            'tag_id'  => 1,
        ]);

        DB::table('pet_tags')->insert([
            'pet_id'  => 4,
            'tag_id'  => 3,
        ]);
    }
}
