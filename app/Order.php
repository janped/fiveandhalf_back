<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'pet_id',
        'quantity',
        'shipDate',
        'status',
        'complete',
    ];

    protected $hidden = [
        'pet_id',
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function pet() {
        return $this->belongsTo('App\Pet');
    }
}
