<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function pets() {
        return $this->belongsToMany('App\Pet', 'pet_tags')->pluck('url');
    }
}
