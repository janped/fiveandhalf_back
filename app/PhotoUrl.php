<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoUrl extends Model
{
    protected $fillable = [
        'pet_id',
        'url',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'pet_id',
        'id',
    ];

    public function pet() {
        return $this->belongsTo('App\Pet');
    }
}
