<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetTag extends Model
{
    protected $fillable = [
        'pet_id',
        'tag_id',
    ];
}
