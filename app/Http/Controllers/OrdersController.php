<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Order[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        try {
            return Order::with('pet')->get();
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(400, 'error', 'Could not handle orders request: ' . $e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(OrderStoreRequest $request)
    {
        $order = new Order($request->all());

        // Check if pet saved successfully
        if(!$order->save()) {
            return response()->apiResponse(400, 'error', 'Could not save order');
        }

        return response()->apiResponse(200, 'success', 'Everything saved correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Order|Order[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function show($id)
    {
        try {
            return Order::with('pet')->find($id);
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(400, 'error', 'Could not handle order request: ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Order::findOrFail($id)->delete()) {
            return response()->apiResponse(400, 'error', 'Order could not be removed');
        }
        return response()->apiResponse(200, 'success', 'Order successfully removed');
    }
}
