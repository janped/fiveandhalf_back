<?php

namespace App\Http\Controllers;

use App\Http\Requests\PetStoreRequest;
use App\Http\Requests\PetUpdateRequest;
use App\Pet;
use App\PhotoUrl;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Pet[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        try {
            return Pet::with('category')->with('tags')->with('photoUrls')->get();
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(400, 'error', 'Could not handle request of pets: ' . $e);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PetStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PetStoreRequest $request)
    {
        $pet = new Pet($request->all());

        // Check if pet saved successfully
        if(!$pet->save()) {
            return response()->apiResponse(405, 'error', 'Could not save pet');
        }

        // Handle pets tags
        $pet->tags()->sync($request->tags);

        // Handle pets photo urls - create a list of photoUrls objects
        $photoUrls = collect($request->photoUrls)->map(function ($url) {
            return new PhotoUrl(['url' => $url]);
        });
        // TODO I would add here additional validation and handle images with storage system

        // Handle pets photo urls - save the association photoUrl <-> pet
        $pet->photoUrls()->saveMany($photoUrls);

        return response()->apiResponse(200, 'success', 'Everything saved correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        try {
            return Pet::with('category')->with('tags')->with('photoUrls')->findOrFail($id);
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(400, 'error', 'Could not handle pet request: ' . $e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PetUpdateRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function update(PetUpdateRequest $request)
    {
        // Update pets unrelated properties
        try {
            $pet = Pet::with('tags')->findOrFail($request->id);
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(404, 'error', 'Pet not found: ' . $e);
        }

        $pet->update($request->all());

        // Update pets tags
        $pet->tags()->sync($request->tags);

        // Check which photos need to be removed
        $removePhotos = collect($pet->photoUrls)->diff($request->photoUrls)->toArray();

        // Check which photos are new
        $newPhotos = collect($request->photoUrls)->diff($pet->photoUrls);

        // Update photo urls
        $photoUrls = $newPhotos->map(function ($url) {
            return new PhotoUrl(['url' => $url]);
        });

        if(sizeof($removePhotos) > 0) {
            if(!$pet->photoUrls()->delete($removePhotos)) {
                return response()->apiResponse(400, 'error', 'Could not delete old pet photos');
            }
        }

        if(sizeof($photoUrls) > 0) {
            if(!$pet->photoUrls()->saveMany($photoUrls)) {
                return response()->apiResponse(400, 'error', 'Could not save new photos');
            }
        }
        return $pet;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $pet = Pet::findOrFail($id);
        }
        catch(ModelNotFoundException $e) {
            return response()->apiResponse(404, 'error', 'Pet not found: ' . $e);
        }

        if(!$pet->delete()) {
            return response()->apiResponse(400, 'error', 'Pet could not be removed');
        }
        return response()->apiResponse(200, 'success', 'Pet successfully removed');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findByTags(Request $request)
    {
        // Replace query string for easier handling (tags will be handled as array)
        $queryParams = [];
        $query = str_replace('tags', 'tags[]', strval($request->getQueryString()));
        parse_str($query, $queryParams);

        // Check if any tags found in query string
        if(!array_key_exists('tags', $queryParams)) {
            return response()->apiResponse(400, 'error', 'Invalid tag value');
        }

        // Find pets by found tags
        $pets = Pet::with('tags')->whereHas('tags', function ($t) use ($queryParams) {
            $t->whereIn('name', $queryParams['tags']);
        }, '=', count($queryParams['tags']))->get();

        return $pets;
    }

    public function updateForm(Request $request, $id)
    {
        // TODO Handle form update of pets
    }

    public function uploadImage(Request $request)
    {
        // TODO Handle image upload
        /**
         * I would use the build-in storage system with keeping records of the pets URLs
         */
    }
}
