<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|integer|exists:pets,id',
            'category_id'   => 'required|integer|exists:categories,id',
            'name'          => 'required|string|min:1|max:255',
            'photo_urls'     => 'present|array',
            'photo_urls.*'   => 'string',
            'tags'          => 'present|array',
            'tags.*'        => 'integer|exists:tags,id',
            'status'        => 'required|string|in:available,pending,sold',
        ];
    }
}
