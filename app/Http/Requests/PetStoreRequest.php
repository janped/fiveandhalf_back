<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PetStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required|integer|min:1|exists:categories,id',
            'name'          => 'required|string|min:1|max:255',
            'photoUrls'     => 'present|array',
            'photoUrls.*'   => 'string',
            'tags'          => 'present|array',
            'tags.*'        => 'integer|exists:tags,id',
            'status'        => 'required|string|in:available,pending,sold',
        ];
    }
}
