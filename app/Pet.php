<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public $timestamps = true;

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'pet_tags');
    }

    public function photoUrls()
    {
        return $this->hasMany('App\PhotoUrl');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Orders');
    }
}
