<?php

use Illuminate\Http\Request;
use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// ORDER ROUTES
Route::apiResource('store/order', 'OrdersController', [
    'except' => ['update']
])->middleware('auth:api');

// PET ROUTES
Route::put('pet', 'PetsController@update')->middleware('auth:api');
Route::apiResource('pet', 'PetsController', [
    'except' => ['update']
])->middleware('auth:api');
Route::get('pet/findByTags', 'PetsController@findByTags')->middleware('auth:api');
Route::post('pet/{id}', 'PetsController@updateForm')->middleware('auth:api');
Route::post('pet/{id}/uploadImage', 'PetsController@uploadImage')->middleware('auth:api');

